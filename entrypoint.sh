#!/usr/bin/env bash
php artisan config:cache
php artisan route:cache
php artisan view:cache
supervisord -d
service nginx start
php-fpm
